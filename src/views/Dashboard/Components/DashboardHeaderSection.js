
import React, { Component } from "react";
import DashboardHeaderCard from "./DashboardHeaderCard";
import ServiceProviders from "./ServiceProviders";
import TodoList from "./TodoList";

class DashboardHeaderSection extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="column">
      <div className="row">
        <DashboardHeaderCard
          count1={"3"}
          title={"Registration Requests"}
          icon1={"1"}
          color1={"flat-color-1"}
          info1={"Due"}
          count2={"5"}
          info2={"Overdue"}
          color2={"flat-color-2"}
        />

        <DashboardHeaderCard
          count1={"3"}
          title={"Service Requests"}
          icon1={"1"}
          color1={"flat-color-1"}
          info1={"Due"}
          count2={"5"}
          info2={"Overdue"}
          color2={"flat-color-2"}
        />
        <DashboardHeaderCard
                  count1={"3"}
                  title={"Issues"}
                  icon1={"1"}
                  color1={"flat-color-1"}
                  info1={"Due"}
                  count2={"5"}
                  info2={"Overdue"}
                  color2={"flat-color-2"}
        />
     

       </div>
      </div>


    );
  }
}

export default DashboardHeaderSection;
