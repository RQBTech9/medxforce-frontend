import { element } from "prop-types";
import React, { Component } from "react";
import { Card, Row } from "react-bootstrap";
import {CgShapeCircle} from 'react-icons/cg';



function ServiceRequests(props) {

  const data =[
    {
      place:"Vijayawada",
      requests:45,
      color:"red"
    },
    {
      place:"Hyderabad",
      requests:80,
      color:"purple"
    },
    {
      place:"Visakhapatnam",
      requests:10,
      color:"blue"
    },
    {
      place:"Guntur",
      requests:3,
      color:"green"
    },
    {
      place:"Rajahmundry",
      requests:5,
      color:"orange"
    },
  ]
  return (
    <div className="">
      <div className="row">
      <div class="col-lg mb-4 col-sm-6 col-md-6">
     <div className="card" style={{width:"30rem",height:"850px",marginLeft:10}}>
     <div className="card-body">
        
        <div className="stat-heading" style={{textAlign:'left',marginBottom:20,fontSize:30}}>Service Requests</div>
   <hr />
            <div className="item1">
                <div style={{float:"left",fontSize:20}}>
                 {
                   data.map((element,index)=>{
                     return <div key={index} className="item-title">
                       <Card style={{display:"table-row",width:250,height:50,padding:200}} >
                       < CgShapeCircle size={20} color={element.color} />
                       {element.place}
                       </Card>
                       </div>
                   })
                 }
                </div>
                <div style={{fontSize:"1.5vw",color:"red",fontWeight:"bold",float:"right"}}>
              {
                   data.map((element,index)=>{
                     return <div key={index} >
                       <div style={{display:"table-row",width:250,height:50,padding:100,}} >
                       {element.requests}
                       </div>
                       </div>
                   })
                 }
              </div>
            </div>
            </div>
            </div> 
        </div>
      </div>
    </div>
  );
}

export default ServiceRequests;