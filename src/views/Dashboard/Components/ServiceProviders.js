import React, { Component } from "react";
import { Card } from "react-bootstrap";
import './StyleSheets/ServiceProvider.css'
import './StyleSheets/ResponsiveCard.css'; 


function ServiceProviders(props) {
  return (
    <div className="col-lg mb-4 col-sm-6 col-md-6">
      
     <div className="card" style={{height:"25rem"}}>
     <div className="card-body">
        
        <div className="stat-heading" style={{textAlign:'left',fontSize:30}}>Service Providers</div>
        
        <br />
            <hr />
            <br />
            
            <div className="item1">
                <div className="item-title" style={{float:"left"}}>
                Active
                </div>
                <div style={{fontSize:"1.5vw",color:"orange",fontWeight:"bold",float:"right"}}>
               {props.count1}
               </div>
            </div>
            <br />
            <br />
            <hr />
            <div className="item2">
                <div className="item-title" style={{float:"left"}}>
                Inactive
                </div>
                <div style={{fontSize:"1.5vw",color:"red",fontWeight:"bold",float:"right"}}>
              {props.count2}
              </div>
            </div>
            <br />
            <br />
            <hr />
            
            <div className="item2">
                <div className="item-title" style={{float:"left"}}>
               At Work
                </div>
                <div style={{fontSize:"1.5vw",color:"green",fontWeight:"bold",float:"right"}}>
              {props.count3}
              </div>
            </div>
            
         
        </div>
      </div>
    </div>
  );
}

export default ServiceProviders;