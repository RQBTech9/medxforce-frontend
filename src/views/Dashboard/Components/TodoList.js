
import React, { Component } from "react";
import "./StyleSheets/TodoList.css";


class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="row">
        <div className="col-lg mb-4 col-sm-6 col-md-6">
          <div className="card" style={{height:"850px"}}>
            <div className="card-body">
              <h4 className="card-title box-title">Today Activity</h4>
              <div className="card-content">
                <div className="todo-list">
                  <div className="tdl-holder">
                    <div className="tdl-content">
                      <ul>
                        <li >
                          <label>
                         
                          <i className="fa fa-user-plus fa-1x" style={{color:'red'}}></i>
                            <span ><b className="card-list-item"> 
                             2 Resigtration
                             </b>
                             <h6 className="card-list-sub">
                               Nursing
                             </h6>
                            </span>
                            
                          </label>

                          
                        </li>
                        <li>
                          <label>
                          <i className="fa fa-info-circle fa-2x" style={{color:'#77e6b9'}}></i>
                            
                            <i className="check-box"></i>
                            <span> <b className="card-list-item">15 Requests</b>
                            <h6 className="card-list-sub">
                               Nursing , Lab Technician
                             </h6></span>
                           
                          </label>
                        </li>
                        
                        <li>
                          <label>
                          <i className="fa fa-history fa-2x" style={{color:'orange'}}></i>
                           
                            <i className="check-box"></i>
                            <span> <b className="card-list-item">1 Registration Approval Pending</b>
                            <h6 className="card-list-sub">
                              Nursing
                            </h6></span>
                            
                          </label>
                        </li>

                        <li>
                          <label>
                          <i class="fa fa-exclamation-triangle fa-2x"></i>
                           
                            <i className="check-box"></i>
                            <span> <b className="card-list-item">Issue Raised</b>
                            <h6 className="card-list-sub">
                              Technicians
                            </h6></span>
                           
                          </label>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        
      </div>
    );
  }
}

export default TodoList;
