

 import React, { Component } from "react";
 

 function DashboardHeaderCard(props) {
   return (
     <div className="col-lg mb-4 col-sm-6 col-md-6">
      <div className="card" style={{padding:20,height:"25rem"}}>
         <div className="card-body">
         
         <div className="stat-heading" style={{textAlign:'left',marginBottom:100,fontSize:30}}>{props.title}</div>
         
         <div className="text-left" style={{float:'left'}}>
         <div className="stat-text">
             <span className="count">
             <div style={{fontSize:"3vw",color:"orange",fontWeight:"bold"}}>
               {props.count1}
               </div></span>
             </div>
         <div className="stat-heading" style={{textAlign:'center'}}>{props.info1}</div>
           
          </div>
          
 
          
         <div className="text-right"  style={{float:'right'}}>
         <div className="stat-text">
         <span className="count">
         <div style={{fontSize:"3vw",color:"red"}}>
               {props.count2}
               </div></span>
             </div>
         <div className="stat-heading" style={{textAlign:'center'}}>{props.info2}</div>
         
          </div>
          
         </div>
       </div>
     </div>
   );
 }
 
 export default DashboardHeaderCard;
 