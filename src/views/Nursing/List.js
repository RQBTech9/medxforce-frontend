

import React, { Component } from "react";
import Breadcrumb from "../../components/BreadCrumb/Breadcrumb";
import { TextInput } from "../../components/TextInput";
import { Button } from "../../components/Button";
import { Card } from "../../components/Card";

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const path = [
      {
        title: "Dashboard",
        url: "med-x-force/dashboard"
      },
      {
        title: "Table",
        url: "med-x-force/nursing"
      }
    ];
    return (
      <div className="main-content-container p-4 container-fluid">
        <Breadcrumb title={"Table"} path={path} />
        <div className="right-panel">
          <div class="row">
            <div class="col-lg-12">
              <Card>
                <table class="table">
                  <thead>
                    <tr>
                      <th>Firstname</th>
                      <th>Lastname</th>
                      <th>Email</th>
                      <th>Email</th>
                      <th>Email</th>
                      <th>Email</th>
                      <th>Email</th>
                      <th>Email</th>
                      <th>Email</th>
                      <th>Email</th>
                      <th>Email</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>John</td>
                      <td>Doe</td>
                      <td>john@example.com</td>
                      <td>john@example.com</td>
                      <td>john@example.com</td>
                      <td>john@example.com</td>
                      <td>john@example.com</td>
                      <td>john@example.com</td>
                      <td>john@example.com</td>
                      <td>john@example.com</td>
                      <th className="button">
                     Active
                      </th>
                    </tr>
                    
                  </tbody>
                </table>
              </Card>
            </div>
            </div>
            </div>
      </div>
    );
  }
}

export default List;
