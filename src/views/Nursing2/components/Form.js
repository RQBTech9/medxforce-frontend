import React from 'react';
import { Dropdown } from 'react-bootstrap';

export const Form = ({ onSubmit }) => {
  return (
    <form onSubmit={onSubmit}>
      <div className="form-group">
        <label htmlFor="name">Name</label>
        <input className="form-control" id="name" />
      </div>
      <div className="form-group">
        <label htmlFor="email">Email address</label>
        <input
          type="email"
          className="form-control"
          id="email"
          placeholder="name@example1.com"
        />
      </div>
      <div className="form-group">
        <Dropdown>
          <Dropdown.Toggle variant="success" id="dropdown-basic">
            Choose Service 
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item href="#/action-1">Nursing</Dropdown.Item>
            <Dropdown.Item href="#/action-2">Home Care</Dropdown.Item>
            <Dropdown.Item href="#/action-3">Nutritional Services</Dropdown.Item>
            <Dropdown.Item href="#/action-4">Physiotherapy</Dropdown.Item>
            <Dropdown.Item href="#/action-5">Speech Therapy</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </div>
      <div className="form-group">
        <button className="form-control btn btn-primary" type="submit">
          Submit
        </button>
      </div>
    </form>
  );
};
export default Form;
