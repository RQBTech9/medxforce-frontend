import React, { Component } from "react";
import { Image ,Button} from "react-bootstrap";
import CreateServiceForm from "./CreateServiceForm";


import './ServiceNursing2.css'

function ServiceNursing2(props) {
  return (
    <div className="col-lg mb-4 col-sm-6 col-md-6">
     
     <div className="card" style={{padding:20}}>
     <div className="card-body">
        <Image class="img-responsive" className="service-image" src= "https://cdn-prod.medicalnewstoday.com/content/images/articles/147/147142/nursing-is-a-varied-and-respected-profession.jpg"  />
        <div className="stat-heading1" style={{textAlign:'left',marginBottom:20,fontSize:30}}>
            <div className="heading">
            <p>
                Service :
            </p>
            
            <h1 className="title" style={{fontSize:25}}> {props.title}</h1>
            </div>
        </div>
        
        <br />
            <hr />
            <br />
            
            <div className="item1">
                <div className="item-title" style={{float:"left"}}>
                Active
                </div>
                <i
                className={`fa fa-${props.icon1} ${props.color1} fa-3x`}  style={{float:"right"}}
                aria-hidden="true"
              >
              {props.count1}
              </i>
            </div>
            <br />
            <br />
            <hr />
            <div className="item2">
                <div className="item-title" style={{float:"left"}}>
                Inactive
                </div>
                <i
                className={`fa fa-${props.icon2} ${props.color2} fa-3x`}  style={{float:"right"}}
                aria-hidden="true"
              >
              {props.count2}
              </i>
            </div>
            <br />
            <br />
            
            
            
            
         
        </div>
      </div>
    </div>
  );
}

export default ServiceNursing2;