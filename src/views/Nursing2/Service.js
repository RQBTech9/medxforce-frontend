import React, { Component } from "react";

import './ServiceNursing2.css'
import ServiceNursing2 from "./ServiceNursing2";
import { Container } from './components/Container';
import { Filler } from './components/Filler';
import './Modal.css';

function Service(props) {
  const triggerText = 'Create Service';
  const onSubmit = (event) => {
    event.preventDefault(event);
    console.log(event.target.name.value);
    console.log(event.target.email.value);
  };
   
  
  return (
    <div>
       <div className="App" class="col-md-12  text-right">
            <Container className="button" triggerText={triggerText} onSubmit={onSubmit} />
        </div>
    <div className="row" id="services">
        <ServiceNursing2 
        title={"Nursing"}
        count1={10}
        count2={10}
        color1={"flat-color-5"}
        color2={"flat-color-3"}
        />
        <ServiceNursing2
        title={"Home Care"}
        count1={10}
        count2={10}
        color1={"flat-color-2"}
        color2={"flat-color-5"}
        /> 
        <ServiceNursing2 
        title={"Nutritional Services"}
        count1={10}
        count2={10}
        color1={"flat-color-3"}
        color2={"flat-color-1"}
        /> 
        </div>
        <div className="row">
        <ServiceNursing2 
        title={"Physiotheraphy"}
        count1={10}
        count2={10}
        color1={"flat-color-3"}
        color2={"flat-color-1"}
        />
        <ServiceNursing2 
        title={"Speech Theraphy"}
        count1={10}
        count2={10}
        color1={"flat-color-3"}
        color2={"flat-color-1"}
        /> 
        <ServiceNursing2 
        title={"Visit Support exe"}
        count1={10}
        count2={10}
        color1={"flat-color-3"}
        color2={"flat-color-1"}
        /> 
        </div>
        </div>
  );
}

export default Service;