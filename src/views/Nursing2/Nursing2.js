import React, { Component } from "react";
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Home from "./Home";
import Service from "./Service";
import List2 from "./List2";
import Requests from "./Requests";
import './Nursing2.css'
import './ServiceNursing2.css'


 
class Nursing2 extends Component {
  
  render() {
   
    return (
      <HashRouter>
        <div>
        <div className="main-content-container p-4 container-fluid">
        <div className="right-panel">
          <div className="card">
            <div className="card-body">
            <div class="blend">
          <ul className="header">
          <div className="row">
          
            <li className="nav-item active ">
                <NavLink className="nav-link" to="/home">
                    <a className="list1"></a></NavLink>
            </li>
            <li className="nav-item ">
                <NavLink className="nav-link" to="/service">
                    <a className="list2">Service</a></NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/list2">
                    <a className="list3">List</a></NavLink>
            </li>
            <li className="nav-item">
                <NavLink className="nav-link" to="/requests">
                    <a className="list4">Requests</a></NavLink>
            </li>
            </div>
            
          </ul>
          </div>
          </div>
          </div>
          </div>
          </div>
        
          <div className="content">
           <Route path="/home" component={Home}/>
            <Route path="/service" component={Service}/>
            <Route path="/list2" component={List2}/>
            <Route path="/requests" component={Requests}/>
          </div>
          
        </div>
      </HashRouter>
    );
  }
}
 
export default Nursing2;