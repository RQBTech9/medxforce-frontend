import React, { Component } from "react";
import Breadcrumb from "../../components/BreadCrumb/Breadcrumb";
import { TextInput } from "../../components/TextInput";
import { Button } from "../../components/Button";
import { Card } from "../../components/Card";
import './List2.css'

class List2 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const path = [
      {
        title: "Dashboard",
        url: "med-x-force/dashboard"
      },
      {
        title: "Table",
        url: "med-x-force/nursing"
      }
    ];
    return (
      <div className="main-content-container p-4 container-fluid">
        <div class="flex-reverse">
        <Button variant="contained"  className="float-right1">INACTIVE</Button>
        <Button variant="contained"  className="float-right2">ACTIVE</Button>
        <Button variant="contained"  className="float-right3">ALL</Button>
        </div>
        <div className="right-panel">
          <div class="row">
            <div class="col-lg-12" >
              <Card>
                <table class="table">
                  <thead>
                    <tr>
                      <th>S.No.</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Experience</th>
                      <th>Working At</th>
                      <th>Specialisation</th>
                      <th>Location</th>
                      <th>Registered Date</th>
                      <th>Valid till</th>
                      <th>Proofs</th>
                      <th>Status</th>
                      
                      
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>01</td>
                      <td>Sandhya</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>20/10/2021</td>
                      <td></td>
                      <th className="button">
                     Active
                      </th>
                    </tr>
                    
                  </tbody>
                </table>
              </Card>
            </div>
            </div>
            </div>
      </div>
    );
  }
}

export default List2


