import logo from './logo.svg';
import './App.css';
import  {SideBar} from  './components/SideMenu';
import { Service } from "./components/Service";
import { Dashboard } from "./components/Dashboard";
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';


function App() {
  return (
    <div className="App">
      <div className="header">
        <div className="logo">
         Sample App
        </div>
            <div className="title-wrapper">
                <div className="title-text">SERVICES</div>
            </div>
        </div>
        <div className="maindiv">
        <Router> 
        <SideBar></SideBar>
          <div className="dashboard-container">   
          <Routes>
          <Route  exact path="/" element={<Dashboard/>}/>
          <Route exact  path="Services"  element={<Service/>}/>
          <Route exact  path="Lab"   element={<Service/>}/>
          <Route exact  path="Ambulance"   element={<Service/>}/>
          <Route exact  path="users"  element={<Service/>}/>
          </Routes>
       
       </div>
       </Router>
        </div>
    </div>
  );
}

export default App;
