import React from "react";
import '../styles/details.css'

export function Deatailsview({data}

){
    return <div className="details-container">
        <div className="image-container">
        <img className="image-details" src={data.image} alt={data.nick}/>
        <div className="img-txt-wrapper">
            <p className="img-txt-srv">
                service:
            </p>
            <p className="img-txt">
                {data.text}
            </p>
        </div>
        </div>    
        <div className="active">
            <p className="active-text">ACTIVE</p>
            <p className="active-number">5</p>
        </div>
        
        <div className="inactive">
            <p className="active-text">INACTIVE</p>
            <p className="inactive-number">3</p>
        </div>    
    </div>
}