

import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import "./StyleSheets/Sidebar.css";
import {GiNurseFemale} from 'react-icons/gi';
import {GiClockwork} from 'react-icons/gi';
import {GiMicroscope} from 'react-icons/gi';
import {FaAmbulance } from 'react-icons/fa';
import {GiDoctorFace} from 'react-icons/gi';
import {FiUsers} from 'react-icons/fi';
import {RiLogoutCircleRLine} from 'react-icons/ri'
import {FaHandHoldingMedical} from 'react-icons/fa';

function Sidebar(props) {
  const [active, setActive] = useState("");
  const [dropdownToggle, setDropDownToggle] = useState(false);
  useEffect(() => {
    setActive(props.location.pathname);
  }, [props.location.pathname, active]);
  

  return (
    <div
      className="sidebar-container border-right main-sidebar"
      id="sticky-sidebar"
    >
      <nav id="sidebar" className={props.toggleClass}>
        <ul className="list-unstyled components">
          <li
            className={active === "/med-x-force/dashboard" ? "active" : null}
          >
            <a href="/med-x-force/dashboard">
              <div className="menu-icon">
                <GiClockwork size="30"/>
              </div>
              <span className="menu-title">Dashboard</span>
            </a>
          </li>

          

          <li
            className={active === "/med-x-force/nursing2" ? "active" : null}
          >
            <a href="/med-x-force/nursing2">
              <div className="menu-icon">
              <GiNurseFemale size="30"/>
              </div>
              <span className="menu-title">Nursing </span>
            </a>
          </li>

          {/* <li className={active === "/widgets" ? "active" : null}>
            <a href="/widgets">
              <div className="menu-icon">
                <i className="fa fa-th-large nav_icon" aria-hidden="true"></i>
              </div>
              <span className="menu-title">Widgets</span>
            </a>
          </li> */}

          <li className={active === "/med-x-force/forms" ? "active" : null}>
            <a href="/med-x-force/forms">
              <div className="menu-icon">
                <GiMicroscope size="30"/>
              </div>
              <span className="menu-title">Lab Technicians</span>
            </a>
          </li>

          <li className={active === "/med-x-force/tables" ? "active" : null}>
            <a href="/med-x-force/tables">
              <div className="menu-icon">
                <FaAmbulance size="30"/>
              </div>
              <span className="menu-title">Ambulance</span>
            </a>
          </li>

          <li className={active === "/med-x-force/pages" ? "active" : null}>
            <a href="/med-x-force/pages">
              <div className="menu-icon">
               < FaHandHoldingMedical size="30"/>
              </div>
              <span className="menu-title">Second Opinion</span>
            </a>
          </li>

          <li className={active === "/med-x-force/charts" ? "active" : null}>
            <a href="/med-x-force/charts">
              <div className="menu-icon">
               < FiUsers size="30"/>
              </div>
              <span className="menu-title">Users</span>
            </a>
          </li>
        </ul>
        <div className="side-menu-footer">
        <div className="user-info">
          
          <h5 className="footer"> < RiLogoutCircleRLine size={30} style={{marginRight:30}}/>Logout</h5>
          
        </div>
      </div>
      </nav>
      
    </div>
  );
}

export default withRouter(Sidebar);
