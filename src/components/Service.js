import React from "react";
import { Deatailsview } from "./Details";
import '../styles/service.css'

const list=[{
    title:'NURSING',
    active:5,
    inactive:3,
    image:'assets/nursing.jpeg',
    nick:"nurse",
    text:'NURSING'

},
{
    title:'HOME CARE',
    active:5,
    inactive:3,
    image:'assets/homecare.jpeg',
    nick:'home',
    text:'HOME CARE',

},
{
    title:'NUTRITIONAL SERVICES',
    active:5,
    inactive:3,
    image:'assets/nutrition.jpeg',
    nick:'food',
    text:'NUTRITIONAL SERVICES'

},
{
    title:'PHYSIOTHERAPY',
    active:5,
    inactive:3,
    image:'assets/physiotherapy.jpeg',
    nick:'therapy',
    text:'PHYSIOTHERAPY'

},
{
    title:'SPEECH THERAPY',
    active:5,
    inactive:3,
    image:'assets/speechtherapy.jpeg',
    text:'SPEECH THERAPY'

},
{
    title:'VISIT SUPPORT EXE',
    active:5,
    inactive:3,
    image:'assets/support.jpeg',
    text:'VISIT SUPPORT EXE'

}
]
export function Service(){

    return(
        <div className="tab-container">
            <div className="tabs">
                <div>Services</div>
                <div>List</div>
                <div>Requests</div>
            </div>
            {/* <div className="text-container"> */}
            <div className="service-text">Create Service</div>
            {/* </div>      */}
            <div className="outlet-container">
                {
                    list.map((item,index)=> (
                    <Deatailsview data={item}></Deatailsview>
                    ) )
                }
            </div>
            
        </div>
    );

}